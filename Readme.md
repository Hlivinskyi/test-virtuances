# Setup Guide to install and run Cypress tests
## Prerequisite

- Node JS needs to be installed on the machine where the tests will be run. You can use the following link to download it: <https://nodejs.org/en/download/>
- Download the Project to the machine or clone it using **git clone** command

## Install Dependencies 

Using CLI (Command Line Interface) go to Project folder inside where **package.json** file locates and run the following command:

```bash
npm install
```

## Run Tests
If you want to run tests through UI (headed mode) use the next command and then click on **iodinesoftware_end_to_end.spec.js** test on appeared UI window
```bash
npm run cy:headed
```

If you want to run tests through CLI (headless mode) use the next command:

```bash
npm run cy:headless
```


