///<reference types="cypress-iframe"

import {navigateTo} from "../support/page_objects/navigationPage";
import {onCareersPage} from "../support/page_objects/careersPage";

describe('Apply to a job', () => {

    it('Verify SDET position availability', () => {
        //Navigate to Careers page
        navigateTo.pageWithSubMenu('Company', 'Careers')
        //Check if particular job role is available in the list
        onCareersPage.checkJobPositionAvailability('Software Development Engineer in Test')
    })

    it('Verify Email validator message when the field is empty', () => {
        //Select particular job role and start applying process
        onCareersPage.selectJobPosition('Software Development Engineer in Test')
        onCareersPage.clickApplyButton()
        //Define authorization to work and sponsorship (Yes/No)
        onCareersPage.setAuthorizedToWorkAndSponsorship('Yes', 'No')
        //Select gender: male, female, genderUnknown and select race: 1 - 8
        onCareersPage.setSelfIdentity('male', '8')
        //Select veteran status: protected-veteran, not-identify, not-identify-identify
        onCareersPage.setVeteranStatus('not-identify')
        //Select disability status: disability, not_disability, declined_disability
        onCareersPage.setDisabilityStatus('declined_disability')
        //Set personal information with first name and last name only
        onCareersPage.setPersonalInformation({
            firstName: 'Serhii',
            lastName: 'Hlivinskyi'
        })
    })

})

