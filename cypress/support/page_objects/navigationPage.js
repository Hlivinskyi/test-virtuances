class NavigationPage {

    pageWithoutSubMenu(mainMenuItem) {
        cy.visit('/')
        cy.contains('a', mainMenuItem).click()
    }

    pageWithSubMenu(mainMenuItem, dropDownMenuItem) {
        cy.visit('/')
        cy.contains('a', mainMenuItem).click()
        cy.contains('a', dropDownMenuItem).click()
    }

}

export const navigateTo = new NavigationPage()
