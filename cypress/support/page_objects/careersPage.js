const loadFrame = () => {
    cy.frameLoaded('#gnewtonIframe')
    return cy.iframe('#gnewtonIframe')
}

class CareersPage {

    checkJobPositionAvailability(jobTitle) {
        loadFrame().contains('a', jobTitle).should('contain', jobTitle)
    }

    selectJobPosition(jobTitle) {
        loadFrame().contains('a', jobTitle).click()
        cy.wait(2000)
    }

    clickApplyButton() {
        loadFrame().find('#gnewtonCareerBody').find('#gnewtonJobDescription').find('[ns-qa="applyBtn"]').click()
        cy.wait(1000)
    }

    setAuthorizedToWorkAndSponsorship(authorizedToWork, sponsorship) {
        loadFrame().find('#gnewtonCareerBody').find('.gnewtonQuestionWrapper').then(item => {
            cy.wrap(item).eq(0).contains(authorizedToWork).click()
            cy.wrap(item).eq(1).contains(sponsorship).click()
        })
        loadFrame().find('#gnewtonCareerBody').find('#saveBtn').click()
        cy.wait(1000)
        loadFrame().find('#gnewtonCareerBody').contains('button', 'Continue').click()
        cy.wait(1000)
    }

    setSelfIdentity(gender, race) {
        loadFrame().find('#gnewtonCareerBody').then(body => {
            cy.wrap(body).find(`#${gender}`).check().should('be.checked')
            cy.wrap(body).find(`#race-${race}`).check().should('be.checked')
            cy.wrap(body).contains('button', 'Continue').click()
            cy.wait(1000)
        })
    }

    setVeteranStatus(status) {
        loadFrame().find('#gnewtonCareerBody').then(body => {
            cy.wrap(body).find(`#${status}`).check().should('be.checked')
            cy.wrap(body).contains('button', 'Continue').click()
            cy.wait(1000)
        })
    }

    setDisabilityStatus() {
        loadFrame().find('#gnewtonCareerBody').then(body => {
            cy.wrap(body).find('#declined_disability').check().should('be.checked')
            cy.wrap(body).contains('button', 'Continue').click()
            cy.wait(1000)
        })
    }

    setPersonalInformation(person) {
        loadFrame().find('#gnewtonCareerBody').then(body => {
            cy.wrap(body).find('input#firstName').type(person.firstName)
            cy.wrap(body).find('input#lastName').type(person.lastName)
            cy.wrap(body).find('[ns-qa="submitBtn"]').click()
            cy.wrap(body).find('#email').then((input) => {
                expect(input[0].validationMessage).to.eq('Please fill out this field.')
            })
        })
    }
}

export const onCareersPage = new CareersPage()
